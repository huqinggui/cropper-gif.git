# cropper-gif

#### 介绍
vue裁剪gif图片，裁剪出来的图片仍然保留动画的案例，直接down下来就可运行


### 演示地址
[https://root181.blog.csdn.net/article/details/117398384](https://root181.blog.csdn.net/article/details/117398384)
[http://195.133.52.118:8081/](http://195.133.52.118:8081/)

#### 安装教程

1.  npm i / yarn

#### 使用说明

1.  npm run serve / yarn serve

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
